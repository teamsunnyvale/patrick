package com.bilal.patrick;

import java.io.*;
import java.net.*;
import java.util.*;

import android.net.Uri;
import android.util.*;
import org.json.*;

public class RestApi {
    //TODO: Add shared pref option to manually override API_URL on a per-install basis
	public static final String API_URL="http://10.0.2.2:8000/api/";
    // bilal: Leave the above line as-is for now; 127.0.0.1 doesn't work with USB debugging.

    public static String LOG_TAG = "ProjectPatrick_API";

    //used to pass error messages from the API to the user
    public enum RestError {
        USERNAME_INVALID(R.string.error_usernameincorrect),
        PASSWORD_INVALID(R.string.error_passwordincorrect),
        NETWORK_ERROR(R.string.error_nonetworkconnection),
        SERVER_ERROR(R.string.error_servererror),
        HTTP_401_ERROR(R.string.error_passwordincorrect);
        private int errorResource;

        private RestError(int resource) {
            errorResource = resource;
        }

        public int getErrorCode()   {
            return errorResource;
        }
    }

    public class RestException extends Exception {
        private RestError error;
        private Integer responseCode;

        public RestException(RestError restError) {
            error = restError;
        }

        public RestError getError() {
            return error;
        }

        public void setResponseCode(Integer code) {
            responseCode = code;
        }

        public Integer getResponseCode() {
            return responseCode;
        }
    }
	
	public String username;
	public String api_token;
	public Boolean logged_in = false;

    private static RestApi instance;

	protected RestApi() {
        //exists so that the class can't be manually instantiated
	}


    //This class is a singleton
    public static RestApi getInstance() {
        if(instance == null) {
            instance = new RestApi();
        }
        return instance;
    }

    public String getToken(String m_username, String m_password) throws RestException {

        Hashtable<String,String> postdata = new Hashtable<String,String>();
        postdata.put("username", m_username);
        postdata.put("password", m_password);


        JSONObject root_obj = get_json_object(doPOST("login/",postdata));

        if(root_obj != null  &&  root_obj.has("token")) {
            try {
                String token = root_obj.getString("token");
                this.username = username;
                this.api_token = token;
                this.logged_in = true;
                return api_token;
            }
            catch (org.json.JSONException e) {
                Log.d(LOG_TAG, e.getMessage());
                throw new RestException(RestError.SERVER_ERROR);
            }
        }
        throw new RestException(RestError.SERVER_ERROR);
    }
	
	public void setCredentials(String m_username, String token) {
		// TODO: Check if token is valid
		username = m_username;
		api_token = token;
		logged_in = true;
	}

    public String createAccount(String firstName, String lastName, String username, String email, String password) throws RestException {
        Hashtable<String,String> postdata = new Hashtable<String,String>();
        postdata.put("first_name", firstName);
        postdata.put("last_name", lastName);
        postdata.put("email", email);
        postdata.put("username", username);
        postdata.put("password", password);

        JSONObject root_obj = get_json_object(doPOST("signup/",postdata));
        if(root_obj != null  &&  root_obj.has("token")) {
            try {
                String token = root_obj.getString("token");
                this.username = username;
                this.api_token = token;
                this.logged_in = true;
                return token;
            }
            catch (org.json.JSONException e) {
                Log.d(LOG_TAG, e.getMessage());
                throw new RestException(RestError.SERVER_ERROR);
            }
        }
        throw new RestException(RestError.SERVER_ERROR);
    }

    public void logout()    {
        username = null;
        api_token = null;
        logged_in = false;
    }
	
	private JSONObject get_json_object(String result) {
		try {
			JSONObject root_obj = (JSONObject) new JSONTokener(result).nextValue();
			return root_obj;
		} catch (Exception e) {
			Log.d(LOG_TAG, e.getMessage());
		}
		
		return null;
	}

	private String doGET(String endpoint) throws RestException {
		String result = "";
				
		try {
			URL url = new URL(API_URL + endpoint);
			
			HttpURLConnection url_connection = (HttpURLConnection) url.openConnection();
			
			// Add auth token
			if (logged_in)
				url_connection.setRequestProperty("Authorization", "Token " + api_token);
			
			url_connection.setRequestMethod("GET");
			url_connection.connect();
			
			result = getString(url_connection.getInputStream());
			if (url_connection.getResponseCode() == 401)
                throw new RestException(RestError.HTTP_401_ERROR);
			if (url_connection.getResponseCode() != 200) {
                RestException e = new RestException(RestError.SERVER_ERROR);
                e.setResponseCode(url_connection.getResponseCode());
                throw e;
            }
			
			url_connection.disconnect();


        } catch (Exception e) {
            if (e instanceof RestException)
                throw ((RestException)e);
            else if (e instanceof FileNotFoundException) { // HTTP 404
                RestException restException = new RestException(RestError.SERVER_ERROR);
                restException.setResponseCode(404);
                throw restException;
            }
            Log.d("ProjectPatrick",e.getMessage());
        }
		return result;
	}
	
	private String doPOST(String endpoint, Hashtable<String,String> data)
	{
		String result = "";
		
		try{

			URL url = new URL(API_URL + endpoint);
		
			HttpURLConnection url_connection = (HttpURLConnection) url.openConnection();
			
			// Add auth token
			if (logged_in)
				url_connection.setRequestProperty("Authorization", "Token " + api_token);
			
			url_connection.setRequestMethod("POST");
			url_connection.setDoOutput(true);
			url_connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			
			Iterator<Hashtable.Entry<String,String>> iterator = data.entrySet().iterator();
			String urlencoded_data = "";
			
			while (iterator.hasNext()) {
				if(urlencoded_data.length() > 0) {
					urlencoded_data += "&";
				}
				
				Hashtable.Entry<String,String> entry = iterator.next();
				urlencoded_data = urlencoded_data + entry.getKey() + "=";
				urlencoded_data += URLEncoder.encode(entry.getValue(), "UTF-8");
			}
			
			DataOutputStream os = new DataOutputStream(url_connection.getOutputStream());
			os.writeBytes(urlencoded_data);
			
			result = getString(url_connection.getInputStream());
			
			if (url_connection.getResponseCode() != 200 && url_connection.getResponseCode() != 201)
				throw new Exception("Did not get a 200 response, reason: " +
					result);
			
			url_connection.disconnect();
			
		} catch (Exception e) {
			Log.d(LOG_TAG,e.getMessage());
		}
		
		return result;
	}
	
	private String getString(InputStream is){
		String result = "";
		
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(is));
			StringBuilder sb = new StringBuilder();
			String line;
				
			while ((line = br.readLine()) != null)
			{
				sb.append(line + '\n');
			}
			
			result = sb.toString();
		} catch (Exception e) {
			Log.d(LOG_TAG,e.getMessage());
		}
		
		return result;
	}

    public boolean doesUserExist(String m_username) {
        //TODO: add API endpoint to do this
        return false;
    }
	
	public JSONObject getUserInfoFromUname(String username) throws RestException {
		
		try {
			JSONObject root_obj = get_json_object(doGET("users/u/" + username + "/"));
			
			return root_obj;
		} catch (Exception e) {

            if (e instanceof RestException) {

                if (((RestException) e).getResponseCode() == 404) { // Invalid username
                    throw new RestException(RestError.USERNAME_INVALID);
                }

                throw (RestException) e;
            }
			Log.d(LOG_TAG, e.getMessage());
		}
		
		return null;
	}

    public JSONObject getUserInfo(String userid) {
        try {
            JSONObject root_obj = get_json_object(doGET("users/" + userid + "/"));

            return root_obj;
        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage());
        }

        return null;
    }
	
	public String sendBubble(User m_receiver, String m_filetype) {
		Hashtable<String,String> postdata = new Hashtable<String,String>();
		postdata.put("receiver", m_receiver.getUsername());
		postdata.put("filetype", m_filetype);
		
		try {
			JSONObject result = get_json_object(doPOST("bubble/create/", postdata));
			
			if (result.has("id"))
				return result.getString("id");
			
		} catch (Exception e) {
			Log.d(LOG_TAG, e.getMessage());
		}
		return null;
	}
	
	public List<Bubble> inboxDump() {
		List<Bubble> result = new ArrayList<Bubble>();
		
		try {
			String output = doGET("inbox/dump/");
			JSONArray array = (JSONArray) new JSONTokener(output).nextValue();
			
			for (int i = 0; i < array.length(); i++) {
				  JSONObject bubble = array.getJSONObject(i);
				  Bubble current_bubble = new Bubble(bubble.getString("id"));
				  result.add(current_bubble);
			}
		} catch (Exception e) {
			Log.d(LOG_TAG, e.getMessage());
		}
		
		return result;
	}

    public JSONObject getBubbleInfo(String id) throws RestException {
        JSONObject result;

        result = get_json_object(doGET("bubble/" + id + "/"));

        return result;
    }

    public void uploadBubble(Uri filename, String bubbleid) {
        File binaryFile = new File(filename.getPath());
        String boundary = Long.toHexString(System.currentTimeMillis()); // Just generate some unique random value.
        String CRLF = "\r\n"; // Line separator required by multipart/form-data.

        try {
            HttpURLConnection connection = (HttpURLConnection)  new URL(API_URL+"bubble/"+bubbleid+"/upload/").openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            if (logged_in)
                connection.setRequestProperty("Authorization", "Token " + api_token);
            PrintWriter writer = null;

            OutputStream output = connection.getOutputStream();
            writer = new PrintWriter(new OutputStreamWriter(output, "UTF-8"), true); // true = autoFlush, important!

              // Send binary file.
            writer.append("--" + boundary).append(CRLF);
            writer.append("Content-Disposition: form-data; name=\"file\"; filename=\"" + binaryFile.getName() + "\"").append(CRLF);
            writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(binaryFile.getName())).append(CRLF);
            writer.append("Content-Transfer-Encoding: binary").append(CRLF);
            writer.append(CRLF).flush();
            InputStream input = null;
            try {
                input = new FileInputStream(binaryFile);
                byte[] buffer = new byte[1024];
                for (int length = 0; (length = input.read(buffer)) > 0;) {
                    output.write(buffer, 0, length);
                }
                output.flush(); // Important! Output cannot be closed. Close of writer will close output as well.
            } finally {
                if (input != null) try { input.close(); } catch (IOException logOrIgnore) {}
            }
            writer.append(CRLF).flush(); // CRLF is important! It indicates end of binary boundary.

            // End of multipart/form-data.
            writer.append("--" + boundary + "--").append(CRLF);

            writer.close();

            // This is just to ensure the request is sent
            Log.d(LOG_TAG, new Integer(connection.getResponseCode()).toString());
        } catch (Exception e) {
          Log.d(LOG_TAG, e.getMessage());
        }
    }

    public String getBubbleUrl(String bubbleId) {
        String response = null;
        try {
            JSONObject rootObj = get_json_object(doGET("bubble/" + bubbleId + "/view/"));

            if (rootObj.has("url"))
                response = rootObj.getString("url");
        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage());
        }
        return response;
    }

    public void saveBubble(String bubbleId, URL url, FileOutputStream fileOutput) {
        try {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(false);
            connection.connect();

            InputStream inputStream = connection.getInputStream();

            int n = -1;
            byte[] buffer = new byte[4096];

            while ((n = inputStream.read(buffer)) != -1) {
                if (n > 0)
                    fileOutput.write(buffer,0,n);
            }

            fileOutput.close();

        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage());
        }
    }

}


