package com.bilal.patrick;


import android.util.Log;

import org.json.JSONObject;

public class User {
    private String username = null;
    private String id = null;

    public User(Integer id) {
        this.id = id.toString();
        RestApi api = RestApi.getInstance();

        try {
            JSONObject other_data = api.getUserInfo(this.id);
            username = other_data.getString("username");
        } catch (Exception e) {
            // TODO: Implement error handling
            // more specifically, throw an exception if the user doesn't exist
            Log.d(RestApi.LOG_TAG,""+e.getMessage());
        }
    }

    public User(String username) throws RestApi.RestException {
        this.username = username;
        RestApi api = RestApi.getInstance();

        try {
            JSONObject other_data = api.getUserInfoFromUname(username);
            id = other_data.getString("id");
        } catch (Exception e) {
            if (RestApi.RestException.class.isInstance(e))
                throw (RestApi.RestException)e;
            Log.d(RestApi.LOG_TAG,e.getMessage());
        }
    }

    public String getUserID() {
        return id;
    }

    public String getUsername() {

        return username;
    }
}
