package com.bilal.patrick;


import android.content.Context;
import android.view.*;
import android.widget.*;

import java.util.ArrayList;

public class BubbleAdapter extends ArrayAdapter<Bubble> {
    private Context context;

    public BubbleAdapter (Context context, int textViewResourceId, ArrayList<Bubble> arrayList) {
        super(context,textViewResourceId,arrayList);
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(android.R.layout.simple_list_item_1, null);
        }

        Bubble item = getItem(position);
        if (item!= null) {
            // My layout has only one TextView
            TextView itemView = (TextView) view.findViewById(android.R.id.text1);
            if (itemView != null) {
                // do whatever you want with your string and long
                itemView.setText(item.getSender().getUsername());
            }
        }

        return view;
    }
}
