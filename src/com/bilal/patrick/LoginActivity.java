package com.bilal.patrick;

import android.app.*;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by bilal on 11/07/13.
 */
public class LoginActivity extends Activity {

    private ProgressDialog pd;
    private TextView errortv;

    private class doLogin extends AsyncTask<String,Void,Boolean> {
        String username;
        RestApi.RestError error;

        @Override
        protected Boolean doInBackground(String... strings) {
            error = null;
            username = strings[0];
            String password = strings[1];

            UserManager um = UserManager.getInstance(LoginActivity.this);
            Boolean result;
            try {
                result = um.login(LoginActivity.this,username,password);
            } catch (RestApi.RestException e) {
                error = e.getError();
                return false;
            }
            return result;
        }

        @Override
        protected void onPostExecute(Boolean logged_in) {
            pd.dismiss();
            if (logged_in) {

                Intent i = new Intent(LoginActivity.this, InboxActivity.class);
                startActivity(i);
                finish();
            } else {
                errortv.setText(getResources().getString(error.getErrorCode()));
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_login);
        errortv = (TextView) findViewById(R.id.textview_login_errors);
        super.onCreate(savedInstanceState);
    }

    public void onLogin(View v) {
        String username = getTextFromEdittext(R.id.edittext_username);
        String password = getTextFromEdittext(R.id.edittext_password);

        pd = ProgressDialog.show(this,"",getString(R.string.progressdialog_logging_in));

        new doLogin().execute(username, password);
    }

    private String getTextFromEdittext(int resourceId) {
        View v = findViewById(resourceId);
        String result = ((EditText)v).getText().toString();
        return result;
    }
}
