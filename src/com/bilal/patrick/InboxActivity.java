package com.bilal.patrick;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.*;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ListView;

import java.util.ArrayList;

public class InboxActivity extends ListActivity {

    private RestApi api;
    private BubbleAdapter bubbleAdapter;

    public static final String PREFS_NAME="ProjectPatrick";

    private class RefreshInbox extends AsyncTask<Void,Void,ArrayList<Bubble>> {

        @Override
        protected void onPreExecute() {
            setProgressBarIndeterminateVisibility(true);
        }

        @Override
        protected ArrayList<Bubble> doInBackground(Void... voids) {

            ArrayList<Bubble> list = (ArrayList<Bubble>) api.inboxDump();
            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<Bubble> arrayList) {

            // TODO: Optimize this
            bubbleAdapter.clear();
            bubbleAdapter.addAll(arrayList);
            bubbleAdapter.notifyDataSetChanged();

            setProgressBarIndeterminateVisibility(false);
        }
    }

	@Override
	protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        UserManager userManager = UserManager.getInstance(this);
        if(userManager.loggedIn == false) {
            Intent intent = new Intent(this, SignupActivity.class);
            startActivity(intent);
        } else { //already logged in, it's safe to access the restAPI
            api = RestApi.getInstance();

            bubbleAdapter = new BubbleAdapter(this,android.R.layout.simple_list_item_1,new ArrayList<Bubble>());
            this.setListAdapter(bubbleAdapter);

            new RefreshInbox().execute();
        }
		super.onCreate(savedInstanceState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.inbox, menu);
		return true;
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new:

                Intent sendBubble = new Intent(this, SendActivity.class);
                startActivity(sendBubble);

                return true;

            case R.id.action_refresh:
                new RefreshInbox().execute();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Bubble bubble = (Bubble) l.getItemAtPosition(position);

        Intent i = new Intent(this, PlayActivity.class);
        i.putExtra("bubble",bubble);
        startActivity(i);
    }

}
