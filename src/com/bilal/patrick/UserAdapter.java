package com.bilal.patrick;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by bilal on 02/08/13.
 */
public class UserAdapter extends ArrayAdapter<User> {
    private Context context;

    public UserAdapter (Context context, int layoutResourceId, ArrayList<User> arrayList) {
        super(context,layoutResourceId,arrayList);
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listitem_send, null);
        }

        User item = getItem(position);
        if (item!= null) {
            // My layout has only one TextView
            TextView itemView = (TextView) view.findViewById(R.id.textview_li_send_username);
            if (itemView != null) {
                // do whatever you want with your string and long
                itemView.setText(item.getUsername());
            }
        }

        return view;
    }
}
