package com.bilal.patrick;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.VideoView;

import java.io.File;

/**
 * Created by bilal on 06/08/13.
 */
public class PlayActivity extends Activity {
    private Bubble bubble;

    private class DownloadBubble extends AsyncTask<Bubble,Void,String> {

        @Override
        protected String doInBackground(Bubble... bubbles) {
            Bubble bubble = bubbles[0];
            String filename = bubble.saveFile(PlayActivity.this);

            return filename;
        }

        @Override
        protected void onPostExecute(String filename) {

            ProgressBar p = (ProgressBar) findViewById(R.id.progressbar_play);
            p.setVisibility(View.INVISIBLE);

            VideoView v = (VideoView) findViewById(R.id.videoview_play);

            File path = new File(getFilesDir(),filename);
            v.setVideoURI(Uri.fromFile(path));
            v.start();

        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_play);

        bubble = getIntent().getParcelableExtra("bubble");

        new DownloadBubble().execute(bubble);

        super.onCreate(savedInstanceState);
    }
}
