package com.bilal.patrick;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONObject;

import java.net.URL;

public class Bubble implements Parcelable {
	private String id;
    private RestApi api;
    public String filetype;
    private String filename;
    private String sender_id;
    private String receiver_id;

    private User sender;
    private User receiver;
	
	// Constructor for an already-created bubble with an id
	public Bubble (String m_id) {
		id = m_id;
        api = RestApi.getInstance();

		fillRestOfInfo();
	}
	
	// Constructor to send a bubble (TODO)
	public Bubble (User m_receiver, String m_filetype) {
        api = RestApi.getInstance();

		id = api.sendBubble(m_receiver, m_filetype);
        filetype = m_filetype;

        // TODO: Fill sender_id and receiver_id

	}

    private Bubble(Parcel p) {
        id          = p.readString();
        filetype    = p.readString();
        filename    = p.readString();
        sender_id   = p.readString();
        receiver_id = p.readString();

        api = RestApi.getInstance();
    }
	
	// TODO: Implement this
	public void delete() {
		
	}
	
	// TODO Implement this
	public void upload(Uri filename) {
		api.uploadBubble(filename,id);
	}
	
	private void fillRestOfInfo(){
        try {
		    JSONObject bubbleInfo = api.getBubbleInfo(id);

            filetype = bubbleInfo.getString("filetype");
            filename = bubbleInfo.getString("filename");
            sender_id = bubbleInfo.getString("sender");
            receiver_id = bubbleInfo.getString("receiver");

            sender = new User(Integer.parseInt(sender_id));
            receiver = new User(Integer.parseInt(receiver_id));
        } catch (Exception e) {
            Log.d("ProjectPatrick",e.getMessage());
        }
	}

    public User getSender() {
        if (sender == null) {
            sender = new User(Integer.parseInt(sender_id));
        }
        return sender;
    }

    public User getReceiver() {
        if (receiver == null) {
            receiver = new User(Integer.parseInt(receiver_id));
        }

        return receiver;
    }

    public String getFilename() {
        return filename;
    }

    public String saveFile(Context context) {
        String _filename = "viewing.mp4";
        try {
            URL s3Url = new URL(api.getBubbleUrl(id));

            api.saveBubble(id,
                    s3Url,
                    context.openFileOutput(_filename, Context.MODE_WORLD_READABLE));

        } catch (Exception e) {
            Log.d(RestApi.LOG_TAG, e.getMessage());
        }
        return _filename;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(filetype);
        parcel.writeString(filename);
        parcel.writeString(sender_id);
        parcel.writeString(receiver_id);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public Bubble createFromParcel(Parcel parcel) {
            return new Bubble(parcel);
        }

        @Override
        public Bubble[] newArray(int i) {
            return new Bubble[i];
        }
    };
}
