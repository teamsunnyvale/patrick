package com.bilal.patrick;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.andreabaccega.formedittextvalidator.Validator;
import com.andreabaccega.widget.FormEditText;

/**
 * Created by frazer on 13/07/13.
 */
public class SignupActivity extends Activity {

    FormEditText firstnameView;
    FormEditText lastnameView;
    FormEditText usernameView;
    FormEditText emailView;
    FormEditText passwordView;
    FormEditText confirmPasswordView;
    TextView errortv;
    private ProgressDialog pd;

    private class doSignup extends AsyncTask<String,Void,Boolean> {
        RestApi.RestError error;

        @Override
        protected Boolean doInBackground(String... strings) {
            error = null;
            String firstName = strings[0];
            String lastName = strings[1];
            String username = strings[2];
            String email = strings[3];
            String password = strings[4];

            boolean result;
            try {
                result = UserManager.getInstance(SignupActivity.this).createAccount(
                        firstName, lastName, username, email, password);
            } catch (RestApi.RestException e) {
                error = e.getError();
                return false;
            }
            return result;
        }

        @Override
        protected void onPostExecute(Boolean created_account) {
            pd.dismiss();
            if (created_account) {
                errortv.setText("it worked");
                Intent i = new Intent(SignupActivity.this, InboxActivity.class);
                startActivity(i);
                finish();
            } else {
                errortv.setText(getResources().getString(error.getErrorCode()));
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_signup);

        firstnameView = (FormEditText) findViewById(R.id.edittext_firstname);
        lastnameView = (FormEditText) findViewById(R.id.edittext_lastname);
        usernameView = (FormEditText) findViewById(R.id.edittext_username);
        emailView = (FormEditText) findViewById(R.id.edittext_email);
        passwordView = (FormEditText) findViewById(R.id.edittext_password);
        confirmPasswordView = (FormEditText) findViewById(R.id.edittext_password_confirm);
        errortv = (TextView) findViewById(R.id.textview_login_errors);

        //check that the username hasn't been taken by another user
        usernameView.addValidator(new Validator(getResources().getString(R.string.error_usernametaken)) {
            @Override
            public boolean isValid(EditText et) {
                return verifyUsernameUnique(usernameView.getText().toString());
            }
        });

        //checks that the two password fields match
        confirmPasswordView.addValidator(new Validator(getResources().getString(R.string.error_passwordmissmatch)) {
            @Override
            public boolean isValid(EditText et) {
                return et.getText().toString().equals(passwordView.getText().toString());
            }
        });

        super.onCreate(savedInstanceState);
    }

    public void OnSignup(View v) {
        //NOTE: if the validation messages display incorrectly, run ./gradlew clean
        FormEditText[] allFields = {firstnameView, lastnameView, usernameView, emailView, passwordView, confirmPasswordView };

        boolean allValid = true;
        for (FormEditText field: allFields) {
            allValid = field.testValidity() && allValid;
        }

        if (allValid) {
            pd = ProgressDialog.show(SignupActivity.this, "", getString(R.string.progressdialog_creating_account));
            new doSignup().execute(firstnameView.getText().toString(), lastnameView.getText().toString(),
                    usernameView.getText().toString(), emailView.getText().toString(), passwordView.getText().toString());
        }
    }

    public void goToLogin(View v) {
        Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean verifyUsernameUnique(String username)   {
        //TODO: verify the username is unique (RestAPI.doGET("/user/"+username)?)
        return !RestApi.getInstance().doesUserExist(username);
    }

}
