package com.bilal.patrick;

import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.NavUtils;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

public class SendActivity extends Activity {

    private static final int RC_ACTIVITY_VIDEO=1337;
    private static final String STORAGE_DIRECTORY="/projectpatrick/";

    private Uri filePath;
    private String fileType="vid"; // For now, only videos
    private UserAdapter userAdapter;

    private class SendBubble extends AsyncTask<String,Void,Boolean>{
        private RestApi.RestException exception;

        @Override
        protected Boolean doInBackground(String... usernames) {
            User receiver = null;

            RestApi api = RestApi.getInstance();

            try {
                receiver = new User(usernames[0]);
            } catch (RestApi.RestException e) {
                exception = e;
                return false;
            }
            String bubbleId = api.sendBubble(receiver,fileType);

            if (bubbleId != null) {
                // TODO: Split this into another background task
                api.uploadBubble(filePath,bubbleId);
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result){
                endActivity();
            } else {
                Toast.makeText(SendActivity.this,
                        getString(exception.getError().getErrorCode()),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void endActivity() {
        finish();
        Toast.makeText(this,"DONE!",Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send);
        // Show the Up button in the action bar.
        setupActionBar();

        Intent video = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        Uri fileUri = Uri.fromFile(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES), "current.mp4")); // TODO: Fix path
        Log.d("ProjectPatrick", "File will be saved to: " + fileUri);

        video.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(video,RC_ACTIVITY_VIDEO);

        userAdapter = new UserAdapter(this,R.layout.listitem_send,new ArrayList<User>());
        ListView lv = (ListView) findViewById(R.id.listview_send);
        lv.setAdapter(userAdapter);
        // TODO: Populate userAdapter with list of "friends", using an asynctask

    }

    /**
     * Set up the {@link android.app.ActionBar}.
     */
    private void setupActionBar() {
        
        getActionBar().setDisplayHomeAsUpEnabled(true);
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.send, menu);
        return true;
    }
    

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. Use NavUtils to allow users
                // to navigate up one level in the application structure. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back
                //
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.action_send:
                sendBubble();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case RC_ACTIVITY_VIDEO:

                if (data == null) {
                    finish();
                    return;
                }

                filePath = data.getData();
                Log.d("ProjectPatrick", filePath.getPath());

                break;
        }
    }

    private void sendBubble() {
        // TODO: Fill this with multiple usernames, add listview support
        String usernames = ((EditText)findViewById(R.id.edittext_send_username)).getText().toString();

        new SendBubble().execute(usernames);
    }

}
