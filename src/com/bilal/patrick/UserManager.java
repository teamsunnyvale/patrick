package com.bilal.patrick;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by frazer on 14/07/13.
 */
public class UserManager {

    private RestApi restApi;
    private String username;
    private String token;
    public Boolean loggedIn = false;

    private static UserManager instance = null;

    private UserManager(Context context) {
        restApi = RestApi.getInstance();
        SharedPreferences preferences = context.getSharedPreferences("preferences", context.MODE_PRIVATE);
        loggedIn = preferences.getBoolean("is_logged_in", false);
        if(loggedIn) {
            username = preferences.getString("username", null);
            token = preferences.getString("api_token", null);
            restApi.setCredentials(username, token);
        }

    }

    public static UserManager getInstance(Context context) {
        if (instance == null)    {
            instance = new UserManager(context);
        }
        return instance;
    }

    public boolean login(Context context, String username, String password) throws RestApi.RestException {
        //this may throw a RestException. The calling activity can use this to display error messages
        token = restApi.getToken(username, password);

        //we've got a valid token, so save it to shared preferences
        SharedPreferences preferences = context.getSharedPreferences("preferences", context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("username", username);
        editor.putString("api_token", token);
        editor.putBoolean("is_logged_in", true);
        editor.commit();
        this.username = username;
        loggedIn = true;
        return true;
    }

    public void logout(Context context)    {
        SharedPreferences preferences = context.getSharedPreferences("preferences", context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove("username");
        editor.remove("api_token");
        editor.putBoolean("is_logged_in", false);
        editor.commit();
    }

    //create and login new account
    public boolean createAccount(String firstName, String lastName, String username, String email, String password) throws RestApi.RestException {
        token = restApi.createAccount(firstName, lastName, username, email, password);
        this.username = username;
        //TODO: save to preferences
        return true;
    }

}
